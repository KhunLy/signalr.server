﻿using Microsoft.AspNetCore.SignalR;
using SignalRServer.Models;

namespace SignalRServer.Hubs
{
    public class MessageHub : Hub
    {
        public void SendMessage(Message message)
        {
            Clients.All.SendAsync("MessageRecieved", message);
        }
    }
}

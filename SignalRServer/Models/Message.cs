﻿namespace SignalRServer.Models
{
    public class Message
    {
        public string Author { get; set; }
        public string Content { get; set; }
    }
}
